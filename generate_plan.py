
from probas import MODELS

verbose = False

if __name__ == '__main__':

    n = 200

    for model_class in MODELS:

        model = model_class()

        if verbose:
            print(model.TEXTS)

        for g in range(10):
            n += 1

            print('## %s-%02d' % (model.__class__.__name__, n))
            success = False
            while not success:
                success = model.generate(gen = n, verbose = verbose)
            print()

        model.print_diversity()
