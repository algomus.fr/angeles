# Co-creative Orchestration of *Angeles* with Layer Scores and Orchestration Plans

This is the companion repository for our coference paper

Maccarini, F., Oudin, M., Giraud, M., Levé, F. (2024). Co-creative Orchestration of Angeles with Layer Scores and Orchestration Plans. In: Johnson, C., Rebelo, S.M., Santos, I. (eds) Artificial Intelligence in Music, Sound, Art and Design. EvoMUSART 2024. Lecture Notes in Computer Science, vol 14633. Springer, Cham. <https://doi.org/10.1007/978-3-031-56992-0_15>

## Content

- Code: generate an orchestration plan with `python generate_plan.py`

- Scores

- Our poster

- Watch the full concert at <https://www.youtube.com/watch?v=7jEyakpjSQc>
