
import random
import math
from collections import defaultdict

class Orch():

    def __init__(self):
        self.LAYERS = list(self.COMBOS.keys())
        self.TEXTS = {z: list(self.COMBOS[z].keys()) for z in self.LAYERS}
        self.STATS = defaultdict(list)

        if self.COMBOS_REMOVE:
            self.remove_combos()

        if self.LOUDNESS:
            self.init_loudness()

    def init_loudness(self):
        self.COMBOS_LOUDNESS = {}

        # print("## Loudness")
        for lay in self.COMBOS:
            for combo, (_, insts, _) in self.COMBOS[lay].items():
                loud = sum([self.INSTRUMENTS_LOUDNESS[i] * p for i, p in insts.items()])
                self.COMBOS_LOUDNESS[combo] = loud
                # print('   %-5s %.2f' % (combo, loud))
            # print()

        self.COMBOS_LOUDNESS_MIN = min(self.COMBOS_LOUDNESS.values())
        self.COMBOS_LOUDNESS_MAX = max(self.COMBOS_LOUDNESS.values())

        self.LOUDNESS_MIN = min(self.LOUDNESS)
        self.LOUDNESS_MAX = max(self.LOUDNESS)

        print("## %s -- Rescaled loudness" % self.__class__.__name__)
        for lay in self.COMBOS:
            for combo in self.COMBOS[lay]:
                loud = self.rescale_loudness(self.COMBOS_LOUDNESS[combo])
                self.COMBOS_LOUDNESS[combo] = loud
                print('   %-5s %.2f' % (combo, loud))
            print()

    def rescale_loudness(self, loud):
        ll = (loud - self.COMBOS_LOUDNESS_MIN) / (self.COMBOS_LOUDNESS_MAX - self.COMBOS_LOUDNESS_MIN)
        return self.LOUDNESS_MIN + ll * (self.LOUDNESS_MAX - self.LOUDNESS_MIN)

    def loudness_prob(self, combo, phrase):
        if not self.LOUDNESS:
            return 1.0
        diff = self.COMBOS_LOUDNESS[combo] - self.LOUDNESS[phrase]
        prob = math.exp(-abs(self.LOUDNESS_LAMBDA * diff))
        # print(combo, "%.3f" % prob, self.LOUDNESS[phrase], self.COMBOS_LOUDNESS[combo])
        return prob

    def remove_combos(self):
        self.COMBOS = self.COMBOS.copy()
        for lay, combos in self.COMBOS_REMOVE.items():
            for combo in combos:
                # print('rm', lay, combo)
                del self.COMBOS[lay][combo]

    # InstList: Wood:Fl.Ob.Cl.Fg|Brass:HrnFI.HrnFII.TrpI.TrpII|Perc:Timp|Strings:Vln1.Vln2.Vla.Vc.Cb
    # [19-27]         <b0ia|0000|0|bbi(ah)h>         h:harm ~a:mel ~b:mel ~i:harm

    def str_orch(self, layers):
        ss = []
        lays = set()
        for f in self.FAMILIES:
            s = ''
            for i in f.split('.'):
                lay = layers[i] if i in layers else None
                if lay:
                    lays.add(lay)
                s += lay[0] if lay else '.'
            ss.append(s)
        return '<%s>' % '|'.join(ss)

    def str_layers(self, layers):
        lays = sorted(['%s:%s' % (lay[0], lay) for lay in layers])
        return ' '.join(lays)

    def generate(self, gen=0, densities=[0.5, 1.0, 4.0], verbose=False):

        if self.INITIAL:
            textures = self.INITIAL.copy()
        else:
            textures = {}
            for lay in self.LAYERS:
                textures[lay] = random.choice(list(self.COMBOS[lay].keys()))

        score_global = 1
        phrase = 0
        out = ''

        out += '## Gen'
        out += ''.join('\t%s%s (%.1f)' % (gen, chr(ord('a')+i), densities[i]) for i in range(len(densities)))
        out += '\n'

        nb_phrases = len(self.LOUDNESS) if self.LOUDNESS else self.NB_PHRASES
        layers_str = {}

        while phrase < nb_phrases:

            textures_old = textures.copy()

            if verbose:
                print('== Phrase', phrase)
            instruments = defaultdict(list)
            for lay in self.LAYERS:

                # Select next states
                if self.TRANSITIONS and (lay in self.TRANSITIONS) and (textures[lay] in self.TRANSITIONS[lay]):
                    ts = list(self.TRANSITIONS[lay][textures[lay]].items())
                else:
                    # Equiprobable
                    ts = [(l, 1.0) for l in self.COMBOS[lay].keys()]

                ss = [p[0] for p in ts]
                # Loudness correction
                ps = [p[1] * self.loudness_prob(p[0], phrase) for p in ts]
                textures[lay] = random.choices(ss, ps)[0]

                # Get instruments
                name, insts, _ = self.COMBOS[lay][textures[lay]]
                if verbose:
                    print(lay, textures[lay], name)
                for i in insts:
                    instruments[i] += [(lay, insts[i])]
            # print(instruments)

            # Score combos
            score = 1
            for lay in self.LAYERS:
                _, __, prefs = self.COMBOS[lay][textures[lay]]
                for layl in self.LAYERS:
                    t = textures[layl]
                    if t in prefs:
                        score *= prefs[t]

            # Discard 0-combos
            if score == 0:
                if verbose:
                    print('* score 0')
                textures = textures_old
                continue

            phrase += 1
            out += '[p%02d]' % phrase

            layers = {}
            # Assign instruments, possibly multiple densities:
            for density in densities:
                for i, prob in instruments.items():

                    if i in layers:
                        # Instrument was already affected, keep it in the same layer
                        continue

                    ls = [p[0] for p in prob]
                    ps = [p[1] for p in prob]

                    # Apply instrument density
                    if random.random() > sum(ps) * density:
                        continue

                    # Assign to one layer
                    z = random.choices(ls, ps)[0]
                    # print(i, prob, sum(ps), '==>', z)
                    layers[i] = textures[z]

                # If some layer is empty, try to assign one available instrument
                for lay in self.LAYERS:
                    if textures[lay] in layers.values():
                        continue
                    available = []
                    for i, prob in instruments.items():
                        if i in layers:
                            continue
                        for (l, p) in prob:
                            if l == lay:
                                available += [(i, p)]
                    # print('!', textures[lay], available)
                    if available:
                        ls = [p[0] for p in available]
                        ps = [p[1] for p in available]
                        i = random.choices(ls, ps)[0]
                        layers[i] = textures[lay]
                    else:
                        if verbose:
                            print('! empty: ', textures[lay])
                        score *= self.SCORE_PENALTY_NO_LAYER
                out += '\t' + self.str_orch(layers)

            out += '\t\t'
            if self.LOUDNESS:
                out += '{%.2f} ' % self.LOUDNESS[phrase-1]
            out += self.str_layers(textures.values())
            layers_str[phrase] = self.str_layers(textures.values())
            out += ' (%.2f)' % score
            out += '\n'
            score_global *= score

            if verbose:
                print()

        # Finish generation

        out += '==> score %.2f' % score_global

        # Discard combos with too low score
        if score_global/1000 < self.SCORE_1K_THRESHOLD:
            if verbose:
                print("score", score_global)
            return False

        print(out)

        for phrase in layers_str:
            self.STATS[phrase] += [layers_str[phrase]]

        return True

    def print_diversity(self):
        print("## %s -- Combo diversity" % self.__class__.__name__)
        for phrase in self.STATS:
            print("[p%02d]" % phrase, len(set(self.STATS[phrase])))