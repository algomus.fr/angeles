
from orch import Orch


class Base(Orch):
    FAMILIES = [
        'Fl.Ob.Cl.Bon',
        'Cor.Trp',
        'Vl1.Vl2.Alt.Vc.Cb',
    ]

    INITIAL = None
    COMBOS_REMOVE = None

    # LOUDNESS = [ 0.2, 0.2, 0.4, 3.0, 3.0, 0.6, 0.2, 0.2, 0.4, 3.0, 3.0, 0.8 ]
    LOUDNESS = [ 0.2, 0.2, 0.5, 1.0, 1.0, 0.5, 0.1, 0.1 ]
    LOUDNESS_LAMBDA = 2.0

    INSTRUMENTS_LOUDNESS = {
        'Fl':1, 'Ob':1, 'Cl':1, 'Bon':1.5,
        'Cor':1.5, 'Trp':2,
        'Vl1':1, 'Vl2':1, 'Alt':1, 'Vc':1, 'Cb':1.5,
    }

    NB_PHRASES = 12  # Overriden by LOUDNESS when present
    SCORE_1K_THRESHOLD = 0.0001
    SCORE_PENALTY_NO_LAYER = 1

class Generic(Base):

    INITIAL = {
        'mel': 'melM2',
        'rhy': 'rhyB',
        'harm': 'harmB',
    }

    COMBOS = {
        'mel': {
            'melH1' : ("Melodie haute (vents)",          {'Fl': .7, 'Ob': .4, 'Cl': .4, 'Trp': .15},
                    {'rhyM2': 5, 'harmM2':5, 'rhyH': 0, 'harmH': 0}),
            'melH2' : ("Melodie haute (cordes)",         {'Vl1': 1.0, 'Vl2': .7 },
                    {'rhyM1': 5, 'harmM1':5, 'rhyH': 0, 'harmH': 0}),
            'melM1' : ("Melodie medium (vents)",         {'Alt': .7, 'Vc': .6, 'Cor': .5, 'Bon': .4, 'Cl': .6},
                    {'rhyM1': 0, 'harmM1': 0}),
            'melM2' : ("Mélodie medium (cordes)",        {'Alt': .7, 'Vc': .6, 'Cor': .5, 'Bon': .4, 'Cl': .6},
                    {'rhyM2': 0, 'harmM2': 0}),
            'melB'  : ("Melodie basse",                  {'Bon': .7, 'Vc': .9, 'Cb': .2},
                    {'rhyB': 0, 'harmB': 0}),
            'melHF' : ("Melodie haute forte",    {'Vl1': .8, 'Vl2': .6, 'Fl': .6, 'Ob': .7, 'Cl': .2, 'Trp': .8 },
                    {'rhyMF': 5, 'harmB': 5}),
            'melMF' : ("Melodie medium forte",   {'Alt': .7, 'Vc': .6, 'Cor': .8, 'Bon': .8, 'Cl': .2},
                    {'harmB': 5})
        },
        'rhy': {
            'rhyH'  : ("Rythme haut",            {'Fl': .5, 'Vl2': .8, 'Vl1': .2, 'Ob':.2, 'Trp': .3},
                    {'harmH': 0}),
            'rhyM1' : ("Rythme medium (vents)",  {'Cl': .6, 'Bon': .3, 'Cor': .2},
                    {'harmM1': 0}),
            'rhyM2' : ("Rythme medium (cordes)", {'Vl2': .7, 'Alt': .7},
                    {'harmM2': 0}),
            'rhyB'  : ("Rythme basse",           {'Vc': .7, 'Cb': .5, 'Bon': .6},
                    {'harmB': 0}),
            'rhyMF' : ("Rythme medium forte",    {'Alt': .6, 'Cl': .5, 'Bon': .2, 'Cor': .1},
                    {'harmMF': 0})
        },
        'harm': {
            'harmH'  : ("Harm haut",             {'Cl': .4, 'Vl1': .6, 'Vl2': .8, 'Fl': .8, 'Ob': .6}, {}),
            'harmM1' : ("Harm medium (vents)",   {'Cl': .5, 'Cor': .6, 'Bon': .2}, {}),
            'harmM2' : ("Harm medium (cordes)",  {'Vl2': .4, 'Alt': .4}, {}),
            'harmB'  : ("Harm basse",            {'Bon': .7, 'Vc': .9, 'Cb': .6}, {}),
            'harmMF' : ("Harm medium forte",     {'Cl': .5, 'Cor': .8, 'Vl2': .8, 'Alt': .8, 'Vl1': .3, 'Bon': .4}, {})
        }
    }

    SCORE_1K_THRESHOLD = 100
    TRANSITIONS = {
    # When a transition is not specified, everything is equiprobable
    'mel': {
        'melH1' : {'melH1': .8, 'melH2': .6, 'melM1': .6, 'melM2': .4, 'melB': .1, 'melHF': .6, 'melMF': .3},
        'melH2' : {'melH1': .6, 'melH2': .8, 'melM1': .4, 'melM2': .6, 'melB': .1, 'melHF': .6, 'melMF': .3},
        'melM1' : {'melH1': .6, 'melH2': .4, 'melM1': .8, 'melM2': .6, 'melB': .1, 'melHF': .4, 'melMF': .6},
        'melM2' : {'melH1': .4, 'melH2': .6, 'melM1': .6, 'melM2': .8, 'melB': .1, 'melHF': .4, 'melMF': .6},
        'melB'  : {'melH1': .3, 'melH2': .3, 'melM1': .3, 'melM2': .3, 'melB': .8, 'melHF': .3, 'melMF': .3},
        'melHF' : {'melH1': .5, 'melH2': .5, 'melM1': .8, 'melM2': .8, 'melB': .1, 'melHF': .8, 'melMF': .3},
        'melMF' : {'melH1': .8, 'melH2': .8, 'melM1': .5, 'melM2': .5, 'melB': .1, 'melHF': .3, 'melMF': .8}
    },
    'rhy': {
        'rhyH'  : {'rhyH': .8, 'rhyM1': .7, 'rhyM2': .7, 'rhyB': .6, 'rhyMF': .6},
        'rhyM1' : {'rhyH': .3, 'rhyM1': .8, 'rhyM2': .4, 'rhyB': .5, 'rhyMF': .6},
        'rhyM2' : {'rhyH': .3, 'rhyM1': .4, 'rhyM2': .8, 'rhyB': .5, 'rhyMF': .6},
        'rhyB'  : {'rhyH': .3, 'rhyM1': .6, 'rhyM2': .6, 'rhyB': .8, 'rhyMF': .7},
        'rhyMF' : {'rhyH': .6, 'rhyM1': .8, 'rhyM2': .8, 'rhyB': .7, 'rhyMF': .8}
    },
    'harm': {
        'harmH' : {'harmH': .8, 'harmM1': .6, 'harmM2': .6, 'harmB': .2, 'harmMF': .4},
        'harmM1': {'harmH': .3, 'harmM1': .8, 'harmM2': .8, 'harmB': .5, 'harmMF': .6},
        'harmM2': {'harmH': .3, 'harmM1': .4, 'harmM2': .8, 'harmB': .5, 'harmMF': .6},
        'harmB' : {'harmH': .6, 'harmM1': .5, 'harmM2': .5, 'harmB': .8, 'harmMF': .7},
        'harmMF': {'harmH': .4, 'harmM1': .5, 'harmM2': .5, 'harmB': .3, 'harmMF': .8}
    }
}



class AngelesTwo(Generic):
    LOUDNESS = [ 0.2, 0.3, 0.5, 0.7, 0.6, 1.0, 0.6, 0.3]
    LOUDNESS_LAMBDA = 4.0

    INITIAL = {
        'mel': 'melBP',
        'harm': 'melH1',
        'iarm': 'iarmM1',
        'pedal': 'pedalP',
    }

    COMBOS = {
        'mel': {
            'melBP'  : ("Melodie basse P",        {'Vc': .3 }, {}),
            'melB1'  : ("Melodie basse 1",        {'Bon': .3, 'Vc': .8}, {}),
            'melB2'  : ("Melodie basse 2",        {'Bon': .8, 'Cl': .5, 'Vc': .4},
                                  {'harmP2': 2, 'iarmP2': 2}),
            'melBF'  : ("Melodie basse F",        {'Vc': .9, 'Cor': .5, 'Bon': .5, 'Alt': .5, 'Trp': .15},
                                  {'harmF': 2, 'iarmF': 2, 'pedF': 2}),
        },

        'harm': {
            'harmP1' : ("haute (vents)",          {'Fl': .5, 'Ob': .2, 'Cl': .2}, {}),
            'harmP2' : ("haute (cordes)",         {'Vl1': .3, 'Vl2': .4 }, {}),
            'harmMF'  : ("haut",                  {'Cl': .2, 'Vl1': .8, 'Vl2': .2, 'Fl': .8, 'Ob': .3}, {}),
            'harmF'  : ("Rythme haut",            {'Fl': .9, 'Vl2': .8, 'Vl1': .9, 'Ob':.2,  'Trp': .3}, {}),
        },
        'iarm': {
            'iarmP1' : ("Iarm medium (vents)",    {'Cl': .5, 'Cor': .5 }, {}),
            'iarmP2' : ("Iarm medium (cordes)",   {'Vl2': .3, 'Alt': .3}, {}),
            'iarmF' :  ("Iarm medium forte",      {'Cl': .5, 'Cor': .8, 'Vl2': .7, 'Alt': .7 }, {})
        },
        'pedal': {
            'pedP'  : ("Pédale basse P",        {'Vc': .9, 'Cb': .2}, {}),
            'pedMF'  : ("Pédale basse MF",      {'Bon': .4, 'Vc': .9, 'Cb': .7, 'Cor': .2}, {}),
            'pedF'  : ("Pédale basse F",      {'Fl': .3, 'Ob': .3, 'Bon': .5, 'Vc': .7, 'Cb': .9, 'Cor': .3}, {}),
        }
    }
    TRANSITIONS = None
    SCORE_1K_THRESHOLD = 0.020

    SCORE_PENALTY_NO_LAYER = .5

SC = .9

class AngelesSixVivace(Base):

    # ??? LOUDNESS = [ 0.2, 0.2, 0.5, 1.0, 1.0, 0.5, 0.1, 0.1 ]
    LOUDNESS = [ 0.2, 0.4, 0.2, 0.5, 0.3, 0.4, 0.2, 0.5, 0.3, 0.5, 0.3, 0.6, 0.4, 0.5, 0.6, 0.7,]
    LOUDNESS_LAMBDA = 4.0

    COMBOS = {
        'mel': {
            'melBP'  : ("Melodie basse P",        {'Vc': .9 }, {}),
            'melB1'  : ("Melodie basse 1",        {'Bon': .7, 'Vc': .9, 'Cb': .2}, {}),
            'melB2'  : ("Melodie basse 2",        {'Bon': .5, 'Cl': .4, 'Vc': .6}, {}),
            'melB3'  : ("Melodie basse 3",        {'Vc': .5, 'Cb': .3, 'Cor': .4}, {}),
            'melBF'  : ("Melodie basse F",        {'Vc': .9, 'Cb': .7, 'Cor': .5},
                        {'1rhyF': 4, '2rhyF': 4, '3rhyF': 4}),
        },
        '1rhy': {
            '1rhyBois' : ("Rythme bois 1",           {'Fl': .3, 'Ob': .1, 'Cl': .2, 'Bon': .15}, {}),
            '1rhyCuiv' : ("Rythme cuivres 1",       {'Cor': .7, 'Trp': .2},
                          {'2rhyCuiv': SC, '3rhyCuiv': SC}),
            '1rhyCord' : ("Rythme cordes 1",         {'Vl1': .5, 'Vl2': .7, 'Alt': .7}, {}),
            '1rhyF'    : ("Rythme F 1",              {'Fl': .5, 'Ob': .4, 'Cl': .2, 'Vl1': .3, 'Vl2': .5, 'Alt': .5, 'Cb': .3   }, {}),
        },
        '2rhy': {
            '2rhyBois' : ("Rythme bois 2",           {'Fl': .4, 'Cl': .4}, {}),
            '2rhyCuiv' : ("Rythme cuivres 2",       {'Cor': .9, 'Trp': .1},
                          {'1rhyCuiv': SC, '3rhyCuiv': SC}),
            '2rhyCord' : ("Rythme cordes 2",         {'Vl2': .9, 'Alt': .9}, {}),
            '2rhyF'    : ("Rythme F 2",              {'Fl': .7, 'Cl': .7, 'Cor': .9, 'Vl1': .1, 'Vl2': .1}, {}),
        },
        '3rhy': {
            '3rhyBois' : ("Rythme bois 3",           {'Cl': .4, 'Bon': .4}, {}),
            '3rhyCuiv' : ("Rythme cuivres 3",        {'Cor': .1, 'Trp': .5},
                          {'1rhyCuiv': SC, '2rhyCuiv': SC}),
            '3rhyCord' : ("Rythme cordes 3",         {'Alt': .7, 'Vc': .6}, {}),
            '3rhyF'    : ("Rythme F 3",              {'Bon': .5, 'Trp': .3, 'Alt': .5, 'Vc': .5, 'Cb': .3}, {})
        }
    }

    TRANSITIONS = None
    SCORE_1K_THRESHOLD = 1000

class AngelesSixAndante(Base):

    LOUDNESS = [ 0.2, 0.2, 0.3, 0.4, 0.3, 0.2, 0.1, 0.1, 0.5, 0.6, 0.3, 0.7 ]

    COMBOS = {
        'mel': {
            'mel1' : ("Melodie bois",          {'Fl': .7, 'Ob': .9, 'Cl': .2}, {}),
            'mel2' : ("Melodie cuivres",       {'Trp': .7, 'Cor': .9}, {}),
            'mel3' : ("Melodie cordes",        {'Vl1': .7, 'Vl2': .9, 'Alt': .2}, {}),
        },
        'harm': {
            'harm1' : ("Harm bois",            {'Fl': .5, 'Ob': .1, 'Cl': .8, 'Bon': .8}, {}),
            'harm2' : ("Harm cuivres",         {'Cor': .9, 'Trp': .1}, {}),
            'harm3' : ("Harm cordes",          {'Vl1': .3, 'Vl2': .8, 'Alt': .8, 'Vc': .6}, {}),
        }
    }

    TRANSITIONS = None

class AngelesSixVivaceNoLoudness(AngelesSixVivace):
    NB_PHRASES = len(AngelesSixVivace.LOUDNESS)
    LOUDNESS = None
    TRANSITIONS = None

    COMBOS_REMOVE = {
                     'mel': [ 'melBF'],
                     '1rhy': [ '1rhyF'],
                     '2rhy': [ '2rhyF'],
                     '3rhy': [ '3rhyF'],
    }
    SCORE_1K_THRESHOLD = 0.001

class AngelesSixAndanteNoLoudness(AngelesSixAndante):
    LOUDNESS = None
    TRANSITIONS = None


MODELS = [
    # Generic(),
    AngelesTwo,
    # AngelesSixVivace,
    # AngelesSixVivaceNoLoudness,
    # AngelesSixAndanteNoLoudness(),
]
